package it.slowik;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Hello world!
 */
public class App {

    public static final String URL = "http://www.bobborst.com/popculture/top-100-songs-of-the-year/?year=%s";

    public static void main(String[] args) throws IOException {
        if (args[0] == null || args[1] == null) {
            System.out.println("correct usage: yearFrom yearTo");
            System.exit(0);
        }
        int j = 1;
        for (int i = Integer.parseInt(args[0]); i < Integer.parseInt(args[1]); i++) {
            final Document document = Jsoup.connect(String.format(URL, i)).get();
            final Elements table = document.getElementsByTag("table");
            final Element element = table.get(0);
            final Elements tbody = element.getElementsByTag("tbody");
            final Element fetchedTBody = tbody.get(0);
            final Elements rows = fetchedTBody.getElementsByTag("tr");
            for (Element row : rows) {
                final Elements cells = row.getElementsByTag("td");
                final Element artist = cells.get(1);
                final Element title = cells.get(2);
                System.out.println(j++ + ". " + artist.text() + " - " + title.text());
            }
        }
    }
}
